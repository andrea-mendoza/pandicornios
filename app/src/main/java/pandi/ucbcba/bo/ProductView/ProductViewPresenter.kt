package pandi.ucbcba.bo.ProductView

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pandi.ucbcba.bo.Database.AppRoomDatabase
import pandi.ucbcba.bo.Database.ProductRepository
import pandi.ucbcba.bo.Database.Products
import pandi.ucbcba.bo.R

class ProductViewPresenter(val view: IContractProductsView.View, val context: Context): IContractProductsView.Presenter {

    var listOfProducts: List<Products> = listOf()

    override fun loadItems() {
    }


    override fun addItem(id: Int) {
        view.showItemsAddedMessage(context.getString(R.string.add_item_shopping_cart))

        GlobalScope.launch {
            val productsDao = AppRoomDatabase.getDatabase(context).productDato()
            val repository = ProductRepository(productsDao)
            repository.updateQuantity(id)
        }

    }


    override fun deleteItem(id: Int) {
        view.showDeleteItemMessage(context.getString(R.string.delete_item_shopping_cart))

        GlobalScope.launch {
            val productsDao = AppRoomDatabase.getDatabase(context).productDato()
            val repository = ProductRepository(productsDao)
            repository.deleteItem(id)
        }

    }

    override fun getItems() {
        TODO("not implemented") //To  body of created functions use File | Settings | File Templates.
    }

    override fun isConnection() : Boolean {
        var hasConnection = isConexion()
        if(!hasConnection){
            view.showInternetError(context.getString(R.string.error_no_internet))
        }

        return hasConnection
    }

    fun isConexion() : Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}