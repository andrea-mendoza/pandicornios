package pandi.ucbcba.bo.ProductView

interface IContractProductsView {
    interface View {
        fun showItemsAddedMessage(message: String)
        fun showDeleteItemMessage(message:String)
        fun showInternetError(message: String)
        fun showErrorItem(message: String)
    }

    interface Presenter{
        fun loadItems()
        fun addItem(id:  Int)
        fun deleteItem(id: Int)
        fun getItems()
        fun isConnection(): Boolean
    }
}