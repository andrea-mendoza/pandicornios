package pandi.ucbcba.bo.ProductView

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_cart.view.*
import kotlinx.android.synthetic.main.product_cart.view.name
import kotlinx.android.synthetic.main.product_cart.view.price
import pandi.ucbcba.bo.Database.Products
import pandi.ucbcba.bo.R

data class ProductList(val id: Int, val name:String, val price: Int, val quantity: Int, val state:Boolean)


class ProductListAdapter(val items: List<Products>, val context: Context, val presenter: IContractProductsView.Presenter): RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.product_cart, parent, false)
        return ProductListViewHolder(
            v
        )

    }


    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ProductListViewHolder, position: Int) {
        val product = items.get(position)
        val picasso = Picasso.get()

        holder.itemView.name.text = product.name
        holder.itemView.price.text = product.price.toString()
        picasso.load( product.photo ).into(holder.itemView.imageViewCart)
        holder.itemView.btn_add_product.setOnClickListener{presenter.addItem(product.id)}
        holder.itemView.btn_delete_product.setOnClickListener{presenter.deleteItem(product.id)}
    }

    class ProductListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        protected var btn_plus: Button
        val price: TextView

        init {
            price = itemView.findViewById(R.id.price) as TextView
            btn_plus = itemView.findViewById(R.id.btn_add_product) as Button
            btn_plus.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if (v != null) {
                if (v.id == btn_plus.id) {
                    ProductsView.modelArrayList.add(Integer.parseInt(price.text.toString()))
                    System.out.println(ProductsView.modelArrayList)

                }
            }
        }
    }

}
