package pandi.ucbcba.bo.ProductView

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_products_view.*
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_products_view.swipeToRefresLayout
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pandi.ucbcba.bo.Database.*
import pandi.ucbcba.bo.LoginFolder.Login
import pandi.ucbcba.bo.MainActivity
import pandi.ucbcba.bo.R
import pandi.ucbcba.bo.ShoppingCarts.ShoppingCart
import pandi.ucbcba.bo.UserMap

class ProductsView : AppCompatActivity(), IContractProductsView.View{
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if( item.itemId === R.id.action_settings) {
            FirebaseAuth.getInstance().signOut();
            startActivity(Intent(this, Login::class.java))
            Toast.makeText(this, "sesion finalizada", Toast.LENGTH_LONG).show()
            return true
        }
        if( item.itemId === R.id.action_patrocinator) {
            startActivity(Intent(this, MainActivity::class.java))
        }
        if( item.itemId === R.id.action_map) {

            startActivity(Intent(this, UserMap::class.java))

            return true
        }
        return false
    }


    private var lista = arrayListOf<Products>()
    lateinit var productViewPresenter: IContractProductsView.Presenter


    var name: String? = null
    var price: Int? = null
    var url: String? = null
    var id_bookstore: Long? = null

    companion object {
        var TAG:String = ProductsView.javaClass.simpleName
        lateinit var modelArrayList: ArrayList<Int>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products_view)

        setSupportActionBar(toolbarProductsView)
        toolbarProductsView.title="Lista de productos"
        supportActionBar.let{
            it?.setDisplayHomeAsUpEnabled(false)
            it?.setDisplayShowHomeEnabled(false)
        }

        productViewPresenter =
            ProductViewPresenter(
                this,
                applicationContext
            )

        //  initialize data
        GlobalScope.launch {
            val productsDao = AppRoomDatabase.getDatabase(applicationContext).productDato()
            val repository = ProductRepository(productsDao)
            repository.deleteProducts()
            repository.insert(Products("Marcadores", 34,"https://images-na.ssl-images-amazon.com/images/I/81d3SJ%2B%2BLHL._AC_SX466_.jpg",0,1))
            repository.insert(Products("Acuarelas", 20,"https://images-na.ssl-images-amazon.com/images/I/71v690GK63L._AC_SX466_.jpg",0,1))
            initializeList(repository.initialize(1))
        }


        // lista.add(Product("Marcadores", 20, 10, true, "https://images-na.ssl-images-amazon.com/images/I/81d3SJ%2B%2BLHL._AC_SX466_.jpg"))
        // lista.add(Product("Acuarelas", 35, 15, true, "https://images-na.ssl-images-amazon.com/images/I/71v690GK63L._AC_SX466_.jpg"))


        swipeToRefresLayout.setOnRefreshListener {
            // TODO: Call backend to update product list
            swipeToRefresLayout.isRefreshing = false
            if(productViewPresenter.isConnection()){
                Log.d(TAG, "Product list is update")
            }
        }

        modelArrayList = arrayListOf<Int>()

        val userListAdapter =
            ProductListAdapter(
                lista,
                this,
                productViewPresenter
            )
        recyclerView.adapter = userListAdapter
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager

    }

    fun initializeList(list: List<Products>){
        for(product in list) lista.add(product)
    }

    override fun showItemsAddedMessage(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }

    override fun showDeleteItemMessage(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }

    override fun showInternetError(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }

    override fun showErrorItem(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun cartOnClickFromProducts(view: View){
        startActivity(Intent(this, ShoppingCart::class.java))
    }

    fun librariesOnClick(view: View){
        startActivity(Intent(this, MainActivity::class.java))
    }

    fun addButtonOnClick(view: View){

    }

}
