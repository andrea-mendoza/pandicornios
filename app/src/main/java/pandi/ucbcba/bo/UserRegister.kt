package pandi.ucbcba.bo

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_user_register.*
import pandi.ucbcba.bo.LoginFolder.Login
import pandi.ucbcba.bo.ProductView.ProductsView

class UserRegister : AppCompatActivity(), IContractRegister.View {

    lateinit var registerPresenter: IContractRegister.Presenter

    //UI elements
    private var etName: EditText? = null
    private var etEmail: EditText? = null
    private var etPassword: EditText? = null
    private var btnCreateAccount: Button? = null
    //Firebase references
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null
    private val TAG = "UserRegister"

    //global variables
    private var name: String? = null
    private var email: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_register)
        initialise()
//        registerPresenter = UserRegisterPresenter(this, applicationContext)
//        btn_register.setOnClickListener {
//            if(isConexion(applicationContext)) {
//                registerPresenter.register(lbl_name.text.toString(),lbl_email.text.toString(),lbl_password.text.toString())
//                val intent = Intent(this,ProductsView::class.java)
//                startActivity(intent)
//            } else {
//                Toast.makeText(this, "No tiene acceso a internet", Toast.LENGTH_LONG).show()
//            }
//        }

    }

    private fun initialise() {
        etName = findViewById<View>(R.id.lbl_name) as EditText
        etEmail = findViewById<View>(R.id.lbl_email) as EditText
        etPassword = findViewById<View>(R.id.lbl_password) as EditText
        btnCreateAccount = findViewById<View>(R.id.btn_register) as Button
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth = FirebaseAuth.getInstance()
        btnCreateAccount!!.setOnClickListener { createNewAccount() }
    }
    private fun createNewAccount() {
        name = etName?.text.toString()
        email = etEmail?.text.toString()
        password = etPassword?.text.toString()
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Enter all details", Toast.LENGTH_SHORT).show()
        }
        mAuth!!
            .createUserWithEmailAndPassword(email!!, password!!)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "createUserWithEmail:success")
                    val userId = mAuth!!.currentUser!!.uid
                    //Verify Email
//                        verifyEmail();

                    val currentUserDb = mDatabaseReference!!.child(userId)
                    currentUserDb.child("firstName").setValue(name)
                        updateUserInfoAndUI()
                } else {
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(this@UserRegister, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
    private fun updateUserInfoAndUI() {
        val intent = Intent(this@UserRegister, Login::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
//    fun locationOnClick(view: View){
//        navigateToLocation()
//    }
    fun locationOnClick(view: View){
        navigateToProductView()
    }

    override fun showErrorName(message: String) {
        lbl_name.error= getString(R.string.error_name_message)
    }

    override fun showErrorPassword(message: String) {
        lbl_password.error= getString(R.string.error_password_message)
    }

    override fun showErrorEmail(message: String) {
        lbl_email.error= getString(R.string.error_email_message)
    }

    override fun showOk(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
//     fun navigateToLocation(){
//         startActivity(Intent(this, location::class.java))
//    }
    fun navigateToProductView(){
        startActivity(Intent(this, ProductsView::class.java))
    }

    fun isConexion( context: Context) : Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}
