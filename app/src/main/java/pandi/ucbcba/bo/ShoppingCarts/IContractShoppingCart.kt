package pandi.ucbcba.bo.ShoppingCarts

import pandi.ucbcba.bo.Database.Products

interface IContractShoppingCart {
    interface View{
        fun showInternetError(message: String)
        fun showItemRemoved(message: String)
        fun updateTotal(total: Int)
        fun showUpdatedMessage(message: String)
        fun showFinishAlert(message: String)
    }

    interface Presenter {
        fun removeItem(id:  Int)
        fun loadShoppingCart(): List<Products>
        fun isConnection(): Boolean
        fun keepShopping()
        fun finishShopping()
    }
}