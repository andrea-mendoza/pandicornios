package pandi.ucbcba.bo.ShoppingCarts

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pandi.ucbcba.bo.Database.AppRoomDatabase
import pandi.ucbcba.bo.Database.ProductRepository
import androidx.core.content.ContextCompat.startActivity
import pandi.ucbcba.bo.Database.Products
import pandi.ucbcba.bo.ProductView.ProductsView
import pandi.ucbcba.bo.R

class ShoppingCartPresenter(val view: IContractShoppingCart.View, val context: Context): IContractShoppingCart.Presenter {
    override fun removeItem(id: Int) {
        GlobalScope.launch {
            val productsDao = AppRoomDatabase.getDatabase(context).productDato()
            val repository = ProductRepository(productsDao)
            repository.removeQuantity(id)
        }
    }

    override fun loadShoppingCart(): List<Products>{
        var list:List<Products> = listOf()
        GlobalScope.launch {
            val productsDao = AppRoomDatabase.getDatabase(context).productDato()
            val repository = ProductRepository(productsDao)
            list = repository.getShoppingCart()
        }

        return list
    }

    override fun isConnection() : Boolean {
        var hasConnection = isConexion()
        if(!hasConnection){
            view.showInternetError(context.getString(R.string.error_no_internet))
        }

        return hasConnection
    }

    override fun keepShopping() {
        startActivity(context,Intent(context, ProductsView::class.java),null)
    }

    override fun finishShopping() {
        view.showFinishAlert(context.getString(R.string.error_under_construction))
    }

    fun isConexion() : Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}