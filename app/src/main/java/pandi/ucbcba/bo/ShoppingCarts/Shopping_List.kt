package pandi.ucbcba.bo.ShoppingCarts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_cart.view.name
import kotlinx.android.synthetic.main.shopping_card.view.*
import pandi.ucbcba.bo.Database.Products
import pandi.ucbcba.bo.R

data class Shopping_List(val btn_remove_item_list:String, val product: Products)

class ShoppingListAdapter(val items: List<Shopping_List>, val context: Context, val presenter: IContractShoppingCart.Presenter): RecyclerView.Adapter<ShoppingListAdapter.ShoppingListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.shopping_card, parent, false)
        return ShoppingListViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ShoppingListViewHolder, position: Int) {
        val shopping = items.get(position)
        val picasso = Picasso.get()


        holder.itemView.btn_remove_item_list.text = shopping.btn_remove_item_list
        holder.itemView.name.text = shopping.product.name
        holder.itemView.priceShpping.text = shopping.product.price.toString()
        holder.itemView.quantity.text = shopping.product.quantity.toString()

        System.out.println(shopping.product.photo)
        picasso.load( shopping.product.photo ).into(holder.itemView.imageViewCart3)
        holder.itemView.btn_remove_item_list.setOnClickListener{presenter.removeItem(shopping.product.id)}
    }

    class ShoppingListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}