package pandi.ucbcba.bo.ShoppingCarts

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_shopping_cart.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pandi.ucbcba.bo.MainActivity
import pandi.ucbcba.bo.Database.AppRoomDatabase
import pandi.ucbcba.bo.Database.ProductRepository
import pandi.ucbcba.bo.Database.Products
import pandi.ucbcba.bo.LoginFolder.Login
import pandi.ucbcba.bo.R
import pandi.ucbcba.bo.UserMap


class ShoppingCart : AppCompatActivity(), IContractShoppingCart.View {

    lateinit var shoppingCartPresenter: IContractShoppingCart.Presenter
    private var lista = arrayListOf<Shopping_List>()
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if( item.itemId === R.id.action_settings) {
            FirebaseAuth.getInstance().signOut();
            startActivity(Intent(this, Login::class.java))
            Toast.makeText(this, "sesion finalizada", Toast.LENGTH_LONG).show()
            return true
        }
        if( item.itemId === R.id.action_patrocinator) {
            startActivity(Intent(this, MainActivity::class.java))
        }
        if( item.itemId === R.id.action_map) {

            startActivity(Intent(this, UserMap::class.java))

            return true
        }

        return false
    }


    companion object {
        var TAG:String = ShoppingCart.javaClass.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)
        shoppingCartPresenter =
            ShoppingCartPresenter(
                this,
                applicationContext
            )
        setSupportActionBar(toolbarShopCart)
        toolbarShopCart.title="Carro de compras"
        supportActionBar.let{
            it?.setDisplayHomeAsUpEnabled(true)
            it?.setDisplayShowHomeEnabled(true)
        }

        // TODO: Change initial text to the value got from backend
        // TODO: Add the logic MVC to add, remove one item and remove all items from shopping cart
        // TODO: Add logic for finish shopping and continue shopping
        // TODO: call backend to update shopping cart

        var list: List<Products> =  listOf()

        GlobalScope.launch {
            val productsDao = AppRoomDatabase.getDatabase(applicationContext).productDato()
            val repository = ProductRepository(productsDao)

            list = repository.getShoppingCart()

            initializeList(repository.getShoppingCart())
        }

        swipeToRefresLayout.setOnRefreshListener {
            swipeToRefresLayout.isRefreshing = false
            if(shoppingCartPresenter.isConnection()){
                Log.d(TAG, "Shopping card is update")
                showUpdatedMessage("La lista actualizada se mostrara en un instante")
                initializeList(shoppingCartPresenter.loadShoppingCart())
            }
        }


        val shoppingListAdapter =
            ShoppingListAdapter(
                lista,
                this,
                shoppingCartPresenter
            )
        recyclerViewShopping.adapter = shoppingListAdapter
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewShopping.layoutManager = linearLayoutManager
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    fun initializeList(list: List<Products>){
        var total = 0
        lista.clear()
        for(product in list) {
            lista.add(
                Shopping_List(
                    "Remover",
                    product
                )
            )
            total += (product.price * product.quantity)
            updateTotal(total)
        }


    }

    override fun showInternetError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showItemRemoved(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateTotal(total: Int) {
        lbl_total_price_amount.text = total.toString()
    }

    override fun showUpdatedMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
    
    override fun showFinishAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        with(builder) {
            setTitle("Alerta")
            setMessage(message)
            setPositiveButton("OK", null)
        }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    fun onClickFinish(view: View){
        shoppingCartPresenter.finishShopping()
    }

    fun keepShopping(view: View){
        shoppingCartPresenter.keepShopping()
    }
}