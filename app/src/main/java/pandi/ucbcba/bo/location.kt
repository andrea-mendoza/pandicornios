package pandi.ucbcba.bo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_location.*
import pandi.ucbcba.bo.ProductView.ProductsView

class location : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        confirm.setOnClickListener({
            val intent = Intent(this,
                ProductsView::class.java)
            startActivity(intent)
        })
    }
}
