package pandi.ucbcba.bo

import android.content.Context


class UserRegisterPresenter(var view: IContractRegister.View, var context: Context): IContractRegister.Presenter {

    override fun register(name: String, email: String, password: String) {

        if(name.isEmpty()){
            view.showErrorName(context.getString(R.string.error_empty_value))
        }
        else if(email.isEmpty()){
            view.showErrorEmail(context.getString(R.string.error_empty_value))
        }
        else if (password.isEmpty()){
            view.showErrorPassword(context.getString(R.string.error_empty_value))
        }
        else{
            view.showOk(context.getString(R.string.successful_register_message))
        }
    }
}