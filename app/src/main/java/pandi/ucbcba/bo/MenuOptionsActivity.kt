package pandi.ucbcba.bo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu_options.*

class MenuOptionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_options)
        labelAddLibrary.setOnClickListener {
            val intent = Intent(this, NewLibrary::class.java)
            startActivity(intent)
        }
    }
}
