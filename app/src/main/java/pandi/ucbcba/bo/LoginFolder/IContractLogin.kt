package pandi.ucbcba.bo.LoginFolder


import com.google.firebase.auth.FirebaseAuth

interface IContractLogin {
    interface View{

        fun showErrorLogin(message: String)
        fun showMessage(message: String)
        fun showUserNameEmpty(message: String)
        fun showPasswordEmpty(message: String)
        fun showInternetError(message: String)
        fun showLoading()
        fun hideLoading()
        fun updateUI()
    }


    interface Presenter{
        fun login(
            username: String,
            password: String,
            auth: FirebaseAuth?,
            login: Login
        )
        fun isConnection(): Boolean
    }
}