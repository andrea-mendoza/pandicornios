package pandi.ucbcba.bo.LoginFolder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import pandi.ucbcba.bo.ProductView.ProductsView
import pandi.ucbcba.bo.R
import pandi.ucbcba.bo.UserMap
import pandi.ucbcba.bo.UserRegister

class Login : AppCompatActivity(), IContractLogin.View {
    private val TAG = "Login"
    //global variables
    private var email: String? = null
    private var password: String? = null

    //UI elements
    private var editTextEmail: EditText? = null
    private var editTextPassword: EditText? = null
    private var btnLogin: Button? = null
    private var btnCreateAccount: TextView? = null

    //Firebase references
    private var mAuth: FirebaseAuth? = null

    lateinit var loginPresenter: IContractLogin.Presenter


    //TextInputLayout til_email, tiedt_email

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginPresenter =
            LoginPresenter(this, applicationContext)
        setContentView(R.layout.activity_login)
        initialise()
    }

    private fun initialise() {
        editTextEmail = findViewById<View>(R.id.lbl_login_email) as EditText
        editTextPassword = findViewById<View>(R.id.lbl_login_password) as EditText
        btnLogin = findViewById<View>(R.id.btn_login) as Button
        btnCreateAccount = findViewById<View>(R.id.createNewUser_text) as TextView
        mAuth = FirebaseAuth.getInstance()
        btnCreateAccount!!
            .setOnClickListener { startActivity(
                Intent(this@Login,
                    UserRegister::class.java)
            ) }
        btnLogin!!.setOnClickListener { loginUser() }

    }

    private fun loginUser() {
        if(loginPresenter.isConnection()){
            email = editTextEmail?.text.toString()
            password = editTextPassword?.text.toString()
            loginPresenter.login(email!!, password!!, mAuth, this)
        }
    }

    override fun updateUI() {
        val intent = Intent(this, ProductsView::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    fun createOnClick(view: View) {
        navigateToCreateAccount()
    }
    override fun showErrorLogin(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_LONG).show()
    }

    override fun showUserNameEmpty(message: String) {
        lbl_login_email.error = message
    }

    override fun showPasswordEmpty(message: String) {
        lbl_login_password.error = message
    }

    override fun showInternetError(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        group.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
        group.visibility = View.VISIBLE
    }

    fun navigateToCreateAccount() {
        startActivity(Intent(this, UserRegister::class.java))
    }

}
