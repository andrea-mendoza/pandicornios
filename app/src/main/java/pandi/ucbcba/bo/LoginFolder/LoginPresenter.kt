package pandi.ucbcba.bo.LoginFolder

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.google.firebase.auth.FirebaseAuth
import pandi.ucbcba.bo.R

class LoginPresenter(val view: IContractLogin.View, val context: Context): IContractLogin.Presenter {
    override fun login(
        username: String,
        password: String,
        auth: FirebaseAuth?,
        login: Login
    ) {
        val isUsernameEmpty = username.isEmpty()
        val isPasswordEmpty = password.isEmpty()

        if(isUsernameEmpty){
            view.showUserNameEmpty(context.getString(R.string.error_empty_value))
        } else if(isPasswordEmpty){
            view.showPasswordEmpty(context.getString(R.string.error_empty_value))
        } else if(!isUsernameEmpty && !isPasswordEmpty){

            auth!!.signInWithEmailAndPassword(username!!, password!!)
                .addOnCompleteListener(login) { task ->

                    if (task.isSuccessful) {
                        view.updateUI()
                    } else {
                        view.showErrorLogin("Usuario o Contraseña incorrectos")
                    }
                }

        }

    }

//    override fun login(userName: String, password: String, authFirebase: FirebaseAuth) {
//        view.showLoading()
//        val runnable = Runnable {
//            view.hideLoading()
//
//            if (userName.isEmpty()) {
//                session.isLogin = false
//                view.showUserNameEmpty(context.getString(R.string.error_empty_value))
//            } else if (password.isEmpty()) {
//                session.isLogin = false
//                view.showPasswordEmpty(context.getString(R.string.error_empty_value))
//            } else if (userName.equals("geeklibrary") && password.equals("geeklibrary")) {
//                session.isLogin = true
//                view.showMessage(context.getString(R.string.login_form_success))
//            } else {
//                session.isLogin = false
//                view.showErrorLogin(context.getString(R.string.error_login_values))
//            }
//        }
//        Handler().postDelayed(runnable, 6000)
//    }

    override fun isConnection() : Boolean {
        var hasConnection = isConexion()
        if(!hasConnection){
            view.showInternetError(context.getString(R.string.error_no_internet))
        }

        return hasConnection
    }

    fun isConexion() : Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}