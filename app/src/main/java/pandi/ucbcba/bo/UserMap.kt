package pandi.ucbcba.bo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import pandi.ucbcba.bo.ProductView.ProductsView

class UserMap : AppCompatActivity()  ,
    OnMapReadyCallback,GoogleMap.OnMarkerClickListener{
    private lateinit var fusedLocationClient:FusedLocationProviderClient

    private lateinit var lastLocation:Location

    companion object {

        private const val LOCATION_PERMISSION_REQUEST_CODE=1

    }
    private lateinit var mMap: GoogleMap



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)




    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMarkerClickListener(this)
        mMap.uiSettings.isZoomControlsEnabled=true
        setUpMap()
    }
    private fun placeMarker(location:LatLng){
        val markerOptions=MarkerOptions().position(location)
        mMap.addMarker(markerOptions)
        mMap.addMarker(
            MarkerOptions()
                .position(LatLng(-17.386570,-66.160187))
                .title("Paris")
        )
        mMap.addMarker(
            MarkerOptions()
                .position(LatLng(-17.383223,-66.168517 ))
                .title("Encantalibros")
        )
        mMap.addMarker(
            MarkerOptions()
                .position(LatLng(-17.372130,-66.167408))
                .title("Josue")
        )
        mMap.addMarker(
            MarkerOptions()
                .position(LatLng(-17.391134,-66.173862))
                .title("Abuelita")
        )
    }
    override fun onMarkerClick(p0: Marker?)=false

    private fun setUpMap(){
        if(ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE)
                return
            }
        mMap.isMyLocationEnabled=true
        mMap.mapType=GoogleMap.MAP_TYPE_NORMAL
        fusedLocationClient.lastLocation.addOnSuccessListener(this) {location ->
            if(location !=null){
                lastLocation=location
                val currentLatLng=LatLng(location.latitude,location.longitude)
                placeMarker(currentLatLng)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,13f))
            }

        }

    }
}
