package pandi.ucbcba.bo

interface IContractRegister {
    interface View {
        fun showErrorName(message: String)
        fun showErrorPassword(message: String)
        fun showErrorEmail(message: String)
        fun showOk(message: String)
    }

    interface Presenter {
        fun register(name: String, email: String, password: String)
    }
}