package pandi.ucbcba.bo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.library.view.*

// Class is called custom for packages names
data class LibraryCustom(val id:Int,val name: String, val ubication: String, val description: String)

class LibraryListAdapter(val items: ArrayList<LibraryCustom>, val context: Context): RecyclerView.Adapter<LibraryListAdapter.LibraryListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.library, parent, false)
        return LibraryListViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: LibraryListViewHolder, position: Int) {
        val library = items.get(position)
        holder.itemView.lbl_library_name.text = library.name
    }

    class LibraryListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

}


