package pandi.ucbcba.bo.Database

import androidx.room.*

@Dao
interface IProductDao {

    @Query("SELECT * FROM products_table")
    fun getList(): List<Products>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(products: Products)

    @Query("DELETE FROM products_table")
    suspend fun deleteAll()

    @Query("SELECT * FROM products_table WHERE id_bookstore = :id")
    fun getProductsFromBookstore(id:Int): List<Products>

    @Query("UPDATE products_table SET quantity = quantity + 1 WHERE id = :id")
    fun updateQuantity (id: Int)

    @Query("SELECT * FROM products_table WHERE id = :id")
    fun getById(id: Int): Products

    @Query("SELECT * FROM products_table WHERE quantity > 0")
    fun getShoppingCart(): List<Products>

    @Query("UPDATE products_table SET quantity = 0 WHERE id = :id")
    fun removeQuantity (id: Int)

    @Query("UPDATE products_table SET quantity = quantity-1 WHERE id = :id")
    fun deleteItem (id: Int)

}