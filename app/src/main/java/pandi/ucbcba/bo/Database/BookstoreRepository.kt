package pandi.ucbcba.bo.Database

class BookRepository(private val bookstoreDao: IBookstoreDao) {

    suspend fun insert(bookstore: Bookstore) {
        bookstoreDao.insert(bookstore)
    }

    fun getListBookstores(): List<Bookstore> {
        return bookstoreDao.getList()
    }

    suspend fun deleteBookstores() {
        bookstoreDao.deleteAll()
    }
}
