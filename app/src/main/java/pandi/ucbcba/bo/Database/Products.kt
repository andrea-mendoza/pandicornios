package pandi.ucbcba.bo.Database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products_table")
data class Products(

    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "price") var price: Int,
    @ColumnInfo(name = "photo") var photo: String,
    @ColumnInfo(name = "quantity") var quantity: Int,
    @ColumnInfo(name = "id_bookstore") var id_bookstore: Long){

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0

}