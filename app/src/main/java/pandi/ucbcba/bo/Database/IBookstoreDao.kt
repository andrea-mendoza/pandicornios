package pandi.ucbcba.bo.Database

import androidx.room.*

@Dao
interface IBookstoreDao {

    @Query("SELECT * FROM bookstore_table")
    fun getList(): List<Bookstore>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(bookstore:Bookstore)

    @Query("DELETE FROM bookstore_table")
    suspend fun deleteAll()
}
