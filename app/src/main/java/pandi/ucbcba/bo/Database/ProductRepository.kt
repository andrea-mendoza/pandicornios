package pandi.ucbcba.bo.Database

class ProductRepository(private val productDao: IProductDao) {

    suspend fun insert(products: Products) {
        productDao.insert(products)
    }

    fun getProductsList(): List<Products> {
        return productDao.getList()
    }

    suspend fun deleteProducts() {
        productDao.deleteAll()
    }

    fun updateQuantity(id: Int){
        productDao.updateQuantity(id)
    }

    fun initialize(id: Int): List<Products>{
        return productDao.getProductsFromBookstore(id)
    }

    fun getById(id: Int): Products{
        return productDao.getById(id)
    }

    fun getShoppingCart(): List<Products> {
        return productDao.getShoppingCart()
    }

    fun removeQuantity(id: Int) {
        productDao.removeQuantity(id)
    }

    fun deleteItem(id: Int){
        productDao.deleteItem(id)
    }
}