package pandi.ucbcba.bo
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pandi.ucbcba.bo.Database.AppRoomDatabase
import pandi.ucbcba.bo.Database.BookRepository
import pandi.ucbcba.bo.Database.Bookstore
import pandi.ucbcba.bo.LoginFolder.Login
import pandi.ucbcba.bo.ProductView.ProductsView
import pandi.ucbcba.bo.ShoppingCarts.ShoppingCart

class MainActivity : AppCompatActivity() {

    companion object {
        var TAG:String = MainActivity.javaClass.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar6)
        toolbar6.title="Patrocinadores"
        supportActionBar.let{
            it?.setDisplayHomeAsUpEnabled(true)
            it?.setDisplayShowHomeEnabled(true)
        }

        var lista: List<Bookstore> = listOf()

        //  initialize data
        GlobalScope.launch {
            val bookstoreDao = AppRoomDatabase.getDatabase(applicationContext).bookstoreDato()
            val repository = BookRepository(bookstoreDao)
            repository.deleteBookstores()
            repository.insert(Bookstore("Encantalibros"))
            repository.insert(Bookstore("Libreria Paris"))
            repository.insert(Bookstore("Libreria Josue"))
            lista = repository.getListBookstores()

        }



        var libraryList = arrayListOf<LibraryCustom>()
        libraryList.add(LibraryCustom(1,"Encantalibros","Av Segunda","Excelente calidad"))
        libraryList.add(LibraryCustom(1,"Paris","Heroinas","Buena calidad y por ahi los precios"))
        libraryList.add(LibraryCustom(1,"Josue","Av Segunda","Excelente calidad"))
        libraryList.add(LibraryCustom(1,"Abuelita","Heroinas","Buena calidad y por ahi los precios"))

        swipe_bookstores.setOnRefreshListener {
//          TODO: call backend to update bookstores-libraries
            swipe_bookstores.isRefreshing = false
            Log.d(TAG, "List of bookstores are update")
            System.out.println(lista)
        }

        val userListAdapter = LibraryListAdapter(libraryList, this)
        recyclerViewLibrary.adapter = userListAdapter
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewLibrary.layoutManager = linearLayoutManager


    }

    fun productsOnClick(view: View){
        startActivity(Intent(this, ProductsView::class.java))
    }

    fun cartOnClick(view: View){
        startActivity(Intent(this, ShoppingCart::class.java))
    }
}
